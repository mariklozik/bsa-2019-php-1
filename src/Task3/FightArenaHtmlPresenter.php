<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fightersList = '';

        foreach($arena->all() as $fighter)
        {
            $fightersList .="<img src=\"{$fighter->getImage()}\">".
                "<p>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</p>";
        }

        return $fightersList;
    }
}
