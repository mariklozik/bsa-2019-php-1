<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];
    private $mostPowerful;
    private $mostHealthy;

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $attack = 0;

        foreach ($this->fighters as $fighter)
        {
            $this->mostPowerful = $fighter->getAttack() > $attack ? $fighter : $this->mostPowerful;

            $attack = $fighter->getAttack();
        }

        return $this->mostPowerful;
    }

    public function mostHealthy(): Fighter
    {
        $health = 0;

        foreach ($this->fighters as $fighter)
        {
            $this->mostHealthy = $fighter->getHealth() > $health ? $fighter : $this->mostHealthy;

            $health = $fighter->getHealth();
        }

        return $this->mostHealthy;
    }

    public function all(): array
    {
        return $this->fighters;
    }
}